var processDocFromUrl = function(url,fun) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {

      if (this.readyState == 4 && this.status == 200) {
        var xmlDoc = xhttp.responseXML;
        fun(xmlDoc);
      }
    };
  xhttp.open("GET", url, true);
// xhttp.setRequestHeader("Access-Control-Allow-Origin", "*");
  xhttp.responseType = "document";
  xhttp.send();
  };

function downloadImg(urlArg,dirName,overridedName){
  var downloadName;
  if (dirName==null){
    downloadName=overridedName;
    // chrome.downloads.download({url:urlArg,filename:overridedName},e=> {});
  }else if(/* dirName is not null and */overridedName==null){
    var slashInd=urlArg.lastIndexOf('/');
    var eqInd=urlArg.lastIndexOf('=');
    var ind;
    (slashInd>eqInd) ? (ind=slashInd) :(ind=eqInd);
    var downloadName=dirName+"/"+urlArg.substring(ind+1);//filename - from / or = to end.
  }else{
    downloadName=dirName+"/"+overridedName;
  }
  chrome.downloads.download({url:urlArg,filename:downloadName},e=> {});
}

function processUrlForSiteWithManyRes(argUrl,callback,downloadDir,preferedWith,preferedHeight,hrefFilter,/*optional*/divSelector){
// callback is called callback(foundUrl,downloadDir);
  console.log("processUrlForSiteWithManyRes "+preferedWith+"x"+preferedHeight);
  if(hrefFilter==null){
    hrefFilter= function(a) {return true};
  }

 processDocFromUrl(argUrl,doc=> {
   //test

   var nodes=doc.getElementsByTagName('a');
   for (var i = 0; i < nodes.length; i++) {
     if (arrayElementInString(['1080p','Full HD','FullHD','FHD'],nodes[i].innerHTML)){
       nodes[i].innerHTML='1920x1080';
     }else if(arrayElementInString(['720p','HD'],nodes[i].innerHTML)){ /// HD wont mismatch with Full hd because i check full hd first.
       nodes[i].innerHTML='1280x720';
     }
   }

  //  var regexRes="[0-9]{3,4}x[0-9]{3,4}";
  var regexRes="[0-9]{3,4}[ ]{0,1}x[ ]{0,1}[0-9]{3,4}";


   var linksInit;
   (divSelector==null)? (linksInit=doc.getElementsByTagName('a')) : (linksInit=$('a',$(divSelector,doc)));

   var links= Array.prototype.slice.call(linksInit);
   links=links.filter(a=>{
     if (a.innerHTML.regexIndexOf(regexRes)== -1){
       return false;
     }else if (hrefFilter(a)){//(arrayElementInString(extensions,a.href.substring(a.href.lastIndexOf(".")))) {  // not sure if that's ok . i should generalize it
       return true;
     }else {
       return false;
     }
   });
   if (links.length==0){
     alert('no images found. sorry');
     return;
   }

   var linksWithRes=links.map(link=> {

     var cutLinkHTML= link.innerHTML.substring(link.innerHTML.regexIndexOf(regexRes));
     // var width=link.innerHTML.substring(0,link.innerHTML.indexOf("x"));
     var indexOfX= cutLinkHTML.indexOf("x");


    //TODO - change charAt ==" " to charAt== [^0-9]

     var endOfWidth;
     //checking if there is space before x
     (cutLinkHTML.charAt(indexOfX-1)==" ") ? (endOfWidth=indexOfX-1) :(endOfWidth=indexOfX)
     var width = cutLinkHTML.substring(0,endOfWidth);;//.replace(" ","");

     var begOfHeight;
     (cutLinkHTML.charAt(indexOfX+1)==" ") ? (begOfHeight=indexOfX+2) : (begOfHeight=indexOfX+1);

     var endOfRes= cutLinkHTML.regexIndexOf("[^0-9]",begOfHeight+1);
     if (endOfRes<0){
       endOfRes=cutLinkHTML.length;
     }
     var height = cutLinkHTML.substring(begOfHeight,endOfRes);
     return {"link":link,"width":parseInt(width),"height":parseInt(height)};
   });
   console.log(linksWithRes);
   // var minDelta=9000;
   var maxRes=15000;
   var minRes=0;

   var closestGEWidthIndices=[];
   var foundGEWidth=maxRes;
   var closestLTWidthIndices=[];
   var foundLTWidth=minRes;



   //width :
   for (var i = 0; i < linksWithRes.length; i++) {
     if(preferedWith <= linksWithRes[i].width&& linksWithRes[i].width<= foundGEWidth){

       if (foundGEWidth!= linksWithRes[i].width){
         closestGEWidthIndices=[];
         foundGEWidth=linksWithRes[i].width;
       }
       closestGEWidthIndices.push(i);
       // console.log(closestGEWidthIndices);
     }else if (foundLTWidth<= linksWithRes[i].width && linksWithRes[i].width <preferedWith){
       if(foundLTWidth!=linksWithRes[i].width){
         closestLTWidthIndices=[];
         foundLTWidth=linksWithRes[i].width;
       }
       closestLTWidthIndices.push(i);
     }

   }

   var indices;
   if (closestGEWidthIndices.length==0){
     if(closestLTWidthIndices.length==0){
       alert("closestGEWidthIndices (and LT) something went wrong"+"needed res= "+preferedWith+"x"+preferedHeight+"\n"+linksWithRes.map(l => {return l.height+l.width;}));
     }else {
       indices=closestLTWidthIndices;
     }
   }else{
     indices=closestGEWidthIndices;
   }

   //height;
   var foundGEHeight=maxRes;
   var foundLTHeight=minRes;
   var closestGEIndex=-1;
   var closestLTIndex=-1;
   var index;
   // i should find closestLTHEIGHT in GE width also :/
   for (var i = 0; i < indices.length; i++) {
     index=indices[i];
     if(preferedHeight <= linksWithRes[index].height&& linksWithRes[index].height<= foundGEHeight){
       if (foundGEHeight!= linksWithRes[index].height){
         closestGEIndex=index;
         foundGEHeight=linksWithRes[closestGEIndex].height;

       }
     }else if (foundLTHeight<= linksWithRes[index].height && linksWithRes[index].height<preferedHeight){
       if(foundLTHeight!= linksWithRes[index].height){
         foundLTHeight=linksWithRes[index].height;
         closestLTIndex=index;
       }
     }
   }
   (closestGEIndex>=0)? (index=closestGEIndex):(index=closestLTIndex);
   if (index<0){
     alert("Cannot find any resolution.")
   }
   callback(linksWithRes[index].link.href,downloadDir);


 });
}
