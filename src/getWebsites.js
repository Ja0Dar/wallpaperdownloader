/// when there are links 240x320 etc

function getWebsites(preferredWidth,preferredHeight,downloadDir,document){

    var websites= [
      {"name":"pexels.com",
      title:"Pexels",
       fun :function(info,tab)
        {
        var imageUrl=info.srcUrl.substring(0,info.srcUrl.indexOf('?'));
        downloadImg(imageUrl,downloadDir);
        }
      },
      {"name":"imgur.com", fun: function(info,tab){
        console.log("imgur!");
         processDocFromUrl(info.linkUrl,doc => {
           var url=doc.getElementsByClassName('post-image')[0].children[0].href;
           downloadImg(url,downloadDir);
         });


     }},
     {"name":"reddit.com", fun: function(info,tab){
       console.log("reddit!");
        processDocFromUrl(info.linkUrl,doc => {
          var url=doc.getElementsByClassName('media-preview-content')[0].children[0].href;
          downloadImg(url,downloadDir);
        });
     }},
     {"name":"deviantart.com",
     title:"Deviantart",
     fun: function(info,tab){
       console.log("deviantart");
       processDocFromUrl(info.linkUrl,doc=> {
        //
        var newName=null;
        var url = doc.getElementsByClassName('dev-page-download')[0];
        if (url == undefined){
          var url = doc.getElementsByClassName('dev-content-normal')[0].src;
        }else{
          url=url.href;
          var end= url.indexOf("?");
          (end<0)? (end=url.length): (end=end);

          newName=url.substring(url.lastIndexOf("/")+1,end);
        }
         downloadImg(url,downloadDir,newName);
       });
     }},
     {"name":"wallpapercave.com", fun : function(info,tab) {
       var url = info.linkUrl;
       processDocFromUrl(url,doc=>{
         var images=doc.getElementsByClassName('wpimg');
         for (var i = 0; i < images.length; i++) {
           downloadImg(images[i].src,downloadDir);
         }
       });

     }},
     {"name":"google.",
     "title":"Google Images",
     "url":"google.com/images",
     "description":"just image search. Works only on not 'zoomed-in'( selected before ) images.",
     fun: function(info,tab){
       var imageUrl;//google link to zooming in on image

      if(info.linkUrl.indexOf('search')>=0){
        alert('Downloading from main google search not supported. You can download easily from google images (click on miniatures).');
        return;
        /*NOT USED CODE
        var currentUrl=tab.url;
        var begOfQ=currentUrl.indexOf('q=')+2;
        var endOfQ=currentUrl.indexOf('&',begOfQ);
        (endOfQ<0) ? (endOfQ=currentUrl.length) :(endOfQ=endOfQ);
        var searchTerm=currentUrl.substring(begOfQ,endOfQ);

        var clickedUrl=urldecode(urldecode(info.linkUrl));
        var begOfFir=clickedUrl.indexOf('fir=')+4;
        var endOfFir=clickedUrl.indexOf(',',begOfFir);


        var id = clickedUrl.substring(begOfFir,endOfFir);
        var imageSearchUrl="https://www.google.pl/search?q="+searchTerm+"&tbm=isch";

        processDocFromUrl(imageSearchUrl,doc=>{
          var links=doc.getElementsByClassName('rg_l');
          // var links=Array.prototype.slice.call(doc.getElementsByClassName('rg_l'));
          console.log(doc);
          for(var i=0;i<links.length;i++){
            if (links[i].children[0].name==id){
              imageUrl=links[i].href;
              // console.log(doc.body);
              console.log(links[i]);
              alert(imageUrl+"id: "+id);
              // break;
            }
          }

        });
        */

        // alert(JSON.stringify(tab));
      }else{
        imageUrl=info.linkUrl;
      }
       ///it works with images.  TODO now make it work in general search
      var startOfUrl=imageUrl.indexOf("imgurl=")+"imgurl=".length;
      var url = imageUrl.substring(startOfUrl,imageUrl.indexOf("&",startOfUrl));
      url=urldecode(url);
      console.log("Google: " +url);
      downloadImg(url,downloadDir);
    }},
    {"name":"simpledesktops.com",fun: function(info,tab){
      var url=info.srcUrl;
      if (url.indexOf('static.simpledesktops.com')== -1){
        consol.log("simpledesktops - wrong click");
        return;
      }
      url=url.substring(0,url.regexIndexOf("\\.[0-9]",0));
      downloadImg(url,downloadDir);
    }},
    {"name":"wallpaperswide.com", fun:(info,tab) => {
      processUrlForSiteWithManyRes(info.linkUrl,downloadImg,downloadDir,preferredWidth,preferredHeight,
        a=> {return arrayElementInString(extensions,a.href.substring(a.href.lastIndexOf(".")))});
    }},
    {"name":"wallpaperscraft.com",fun:function(info,tab){
      var clickedLink=info.linkUrl;
      var docFunc= doc=> {
        var url=doc.getElementsByClassName('wd_zoom')[0].children[0].src;
        downloadImg(url,downloadDir);
      };
      if (clickedLink.indexOf("/download/")>0){
        processDocFromUrl(clickedLink,docFunc);
      }else {
        processUrlForSiteWithManyRes(info.linkUrl,(websiteUrl,nothing)=>{processDocFromUrl(websiteUrl,docFunc)},
        downloadDir,preferredWidth,preferredHeight);
      }
    }},
    {"name":"tapeciarnia.pl",fun: function(info,tab){
        processDocFromUrl(info.linkUrl,doc =>{
          var url= doc.getElementsByClassName('pobierz')[0].href;
          downloadImg(url,downloadDir);
        });



    }},
    {"name":"superwall.us",fun: (info,tab)=> {
      processDocFromUrl(info.linkUrl, doc=>{
        var url=doc.getElementsByTagName('img')[0].src
        var tn = 'thumbnail';
        var tnIndex=url.indexOf(tn);
        var urlstart=url.substring(0,tnIndex);
        var urlend=url.substring(tnIndex+tn.length);
        var url = urlstart+"wallpaper"+urlend;
        downloadImg(url,downloadDir);
      })
    }},
    {"name":"motaen.com",fun: (info,tab)=>{

      var callback=(url,dir)=> {
        processDocFromUrl(url,doc=>{
          var durl = doc.getElementById('download-img').src;
          downloadImg(durl,dir);
        });
      }
      processUrlForSiteWithManyRes(info.linkUrl,callback,downloadDir,preferredWidth,preferredHeight,/*filter*/null,'.download-pad');

    }},
    {"name":"wallpaperup.com", fun:(info,tab)=>{
      var tmpUrl=info.linkUrl;
      tmpUrl= tmpUrl.substring(tmpUrl.indexOf('.com/')+'.com/'.length);
      var id = tmpUrl.substring(0,tmpUrl.indexOf('/'));
      var url="http://www.wallpaperup.com/wallpaper/download_custom/"+id;
      //A bit clumsy?
      processDocFromUrl(url,doc=>{
        var name=$('.big-thumb-wrp',doc)[0].children[0].children[0].alt+'.jpg';
        var callback=(link,dir)=>{
          if(dir!=null){
            downloadImg(link,dir,name);
          }else{
            downloadImg(link);
          }
        };
        processUrlForSiteWithManyRes(url,callback,downloadDir,preferredWidth,preferredHeight);
      });
    }},
    {"name":"hdw.eweb4.com",fun:(info,tab)=> {
          processDocFromUrl(info.linkUrl,doc =>{
            var downloadUrl;
            if(info.linkUrl.indexOf('out')>=0){
              downloadUrl=doc.getElementsByClassName('btn blue dlb')[0].href;
            }else if(info.linkUrl.indexOf('wallpapers')>=0) {
              downloadUrl=doc.getElementsByClassName('btn blue med')[0].href;
            }

            if(downloadUrl!=null){
              downloadImg(downloadUrl,downloadDir);
            }else{
              alert('Error with parsing url has occured. Possible cause : changes in website');
            }
          });
    }},
    {name:"hdwallpapers.in",fun:(info,tab)=>{
      processUrlForSiteWithManyRes(info.linkUrl,downloadImg,downloadDir,preferredWidth,preferredHeight,null/*filter*/,'.wallpaper-resolutions');
    }}
    ];
  return websites;


}
