var widthDOM,heightDOM,dirDOM;
var storage= chrome.storage.local;

var isNumeric = function( obj ) {
    return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
}


var changePreferences= function() {
  if(! (isNumeric(widthDOM.value)&& isNumeric(heightDOM.value))){
    alert('Width and Height have to be numbers');
    storage.get('preferredWidth',obj=>{widthDOM.value=obj.preferredWidth});
    storage.get('preferredHeight',obj=>{heightDOM.value=obj.preferredHeight});
    // heightDOM.value=localStorage.preferredHeight;
    return;
  }

  // localStorage.preferredWidth=widthDOM.value;
  // localStorage.preferredHeight=heightDOM.value;
  storage.set({"preferredHeight":heightDOM.value,"preferredWidth":widthDOM.value});

  var dirToSave
  var tmp=arrayElementInString(['*',
                                "\\",
                                "?",
                                "%",
                                "|",
                                "<",
                                ">",
                                "\"",
                                ":"],dirDOM.value);

  if(tmp){
    displayToast('Illegal value in directory name');
    return;
  }
  if (dirDOM.value=="" || dirDOM.value==null){
    dirToSave=null;
  }else{
    dirToSave=dirDOM.value;
  }
storage.set({"downloadDir":dirToSave});
displayToast("Saved: "+widthDOM.value+"x"+heightDOM.value+". Download directory: "+dirToSave);


}

function loadPreferencesToInputs(){
// heightDOM.value=localStorage.preferredHeight;
// widthDOM.value=localStorage.preferredWidth;
storage.get('preferredWidth',obj=>{widthDOM.value=obj.preferredWidth});
storage.get('preferredHeight',obj=>{heightDOM.value=obj.preferredHeight});
storage.get('downloadDir',obj=>{dirDOM.value=obj.downloadDir});
}

function displayToast(text){
  var snack=document.getElementById('snackbar');
  var oldVal=snack.innerHTML;
  snack.innerHTML=text;
  snack.className="show";
  setTimeout(()=>{
    snack.className=snack.className.replace("show","");
    snack.innerHTML=oldVal;
  },2300);
}

//////////////////// SET UP Site
window.onload=e=> {
  widthDOM = document.getElementById('width');
  heightDOM = document.getElementById('height');
  dirDOM=document.getElementById('downloadDir');

  storage.get(['preferredHeight','preferredWidth'],obj=>{
    if (obj.preferredWidth==null || obj.preferredHeight==null){
      storage.set({"preferredHeight":1080,"preferredWidth":1920});
    }
  });
  loadPreferencesToInputs();
  document.getElementById('saveRes').addEventListener("click",changePreferences);
  // var preferredWidth,preferredHeight,downloadDir;//I shouldnt need it

//print websites
  var websites=getWebsites(1,2,"1");
  var a = document.createElement('h3');
  a.textContent="Supported Websites";
  var ul = document.createElement('ul');
  document.body.appendChild(a);

  var url, title, description;
  var li,a;
  for (var i = 0; i < websites.length; i++) {
    li = document.createElement('li');
    a=document.createElement('a');


    //get url for website
    (websites[i].url==null) ? (url=websites[i].name) :(url=websites[i].url);
    if(url.substring(0,4)!="http"){
      url="http://"+url;
    }

    //get title for website li
    (websites[i].title==null) ? (title=websites[i].name) : (title= websites[i].title);

    description= websites[i].description;


    a.textContent=title;
    a.href=url;


    li.appendChild(a);
    if(description!=null){
      var desc= document.createElement('i');
      desc.innerHTML="   "+description;
      li.appendChild(desc);
    }

    ul.appendChild(li);
  }
  document.body.appendChild(ul);
};
