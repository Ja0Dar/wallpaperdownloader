
var clickHandler= function(info,tab,websites){
  var linkEnd=info.linkUrl.substring(info.linkUrl.lastIndexOf('.')+1,info.linkUrl.length);
  for (var i = 0; i < extensions.length; i++) {
    if (extensions[i]== linkEnd){
      downloadImg(info.linkUrl,downloadDir);
      return
    }
  }
  console.log("clickHandler: info : \n"+JSON.stringify(info));

  var websiteUrl= info.linkUrl;
  // websites=getWebsites();

  ////////End of website list
  websiteUrl=websiteUrl.substring(websiteUrl.indexOf('//')+2);
  websiteUrl=websiteUrl.substring(0,websiteUrl.indexOf('/'));
  // var fun;
  for (var i = 0; i < websites.length; i++) {
    if (websiteUrl.indexOf(websites[i].name)!== -1 ){
      websites[i].fun(info,tab);
      // fun=new Function (websites[i].fun);
      // fun(info,tab);
      break;
    }
  }



}

//reloads settings and executes callback with info,tab
function reloadSettings(callback,info,tab){
  var storage= chrome.storage.local;

  storage.get(['preferredHeight','preferredWidth','downloadDir'],obj=>{

    if (obj.preferredWidth==null || obj.preferredHeight==null){
      storage.set({"preferredHeight":1080,"preferredWidth":1920});
      preferredWidth=1920;
      preferredHeight=1080;
    }else{
      preferredHeight=parseInt(obj.preferredHeight);
      preferredWidth=parseInt(obj.preferredWidth);
    }
    downloadDir=obj.downloadDir;

    websites=getWebsites(preferredWidth,preferredHeight,downloadDir);
    callback(info,tab,websites);
  });
}



var clickHandlerWrapper=function(info,tab){

  reloadSettings(clickHandler,info,tab);
}

var websites;
var downloadDir,preferredWidth,preferredHeight;
var extensions=['bmp','jpg','jpeg','png','gif'];
var con_menu = chrome.contextMenus.create({"title":"Download Wallpaper","contexts":["link"],"onclick":clickHandlerWrapper});
