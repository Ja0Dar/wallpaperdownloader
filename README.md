Google Chrome extension for downloading wallpapers from specific websites.
### What does it do?
On supported websites  - just right-click on thumbnail and download wallpaper.
You can set preferred resolution and download directory ( in "Downloads").
Preferred resolution is used if website provides multiple resolutions for a wallpaper.



#### Installing :
Chrome Settings -> Extensions  -> Check "Developer Mode" -> Load extension without package. ->  Point to directory with this repository.
